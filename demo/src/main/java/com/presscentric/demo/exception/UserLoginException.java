package com.presscentric.demo.exception;

public class UserLoginException extends RuntimeException {

    public UserLoginException(String message) {
        super(message);
    }
}