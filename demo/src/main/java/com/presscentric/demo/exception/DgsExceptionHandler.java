package com.presscentric.demo.exception;

import com.netflix.graphql.dgs.exceptions.DefaultDataFetcherExceptionHandler;
import com.netflix.graphql.types.errors.TypedGraphQLError;
import graphql.execution.DataFetcherExceptionHandlerParameters;
import graphql.execution.DataFetcherExceptionHandlerResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Component
public class DgsExceptionHandler extends DefaultDataFetcherExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(DgsExceptionHandler.class);

    @Override
    public CompletableFuture<DataFetcherExceptionHandlerResult> handleException(DataFetcherExceptionHandlerParameters handlerParameters) {
        Throwable exception = handlerParameters.getException();

        TypedGraphQLError.Builder errorBuilder;
        if (exception instanceof EntityNotFoundException) {
            errorBuilder = TypedGraphQLError.newNotFoundBuilder().message(exception.getMessage());
        } else {
            errorBuilder = TypedGraphQLError.newInternalErrorBuilder().message("An error occurred: " + exception.getMessage());

            LOG.error("Unhandled exception at {}: ", handlerParameters.getPath(), exception);
        }

        return CompletableFuture.completedFuture(DataFetcherExceptionHandlerResult.newResult()
                .error(errorBuilder.path(handlerParameters.getPath()).build())
                .build());
    }
}






