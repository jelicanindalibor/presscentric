package com.presscentric.demo.graphql;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.DgsQuery;
import com.presscentric.demo.dto.CreateUserInput;
import com.presscentric.demo.dto.UpdateUserInput;
import com.presscentric.demo.entity.User;
import com.presscentric.demo.security.payload.request.LoginRequest;
import com.presscentric.demo.security.payload.response.LoginResponse;
import com.presscentric.demo.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;

@DgsComponent
@Validated
public class UserGraphQLResolver {

    @Autowired
    private UserService userService;

    @DgsQuery
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Iterable<User> users() {
        return userService.getAllUsers();
    }

    @DgsQuery
    @Secured("ROLE_ADMIN")
    public User user(Long id) {
        return userService.getUserById(id);
    }

    @DgsMutation
    @Secured("ROLE_ADMIN")
    public User createUser(@Valid CreateUserInput input) {
        return userService.createUser(input);
    }

    @DgsMutation
    @Secured("ROLE_ADMIN")
    public User updateUser(Long id, @Valid UpdateUserInput input) {
        return userService.updateUser(id, input);
    }

    @DgsMutation
    @Secured("ROLE_ADMIN")
    public User deleteUser(Long id) {
        return userService.deleteUser(id);
    }

    @DgsMutation
    public LoginResponse login(@Valid LoginRequest loginRequest) {
        return userService.login(loginRequest);
    }
}