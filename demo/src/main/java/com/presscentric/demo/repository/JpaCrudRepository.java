package com.presscentric.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface JpaCrudRepository<T, ID> extends JpaRepository<T, ID> {

}
