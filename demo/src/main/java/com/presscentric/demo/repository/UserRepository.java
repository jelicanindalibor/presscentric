package com.presscentric.demo.repository;

import com.presscentric.demo.entity.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaCrudRepository<User, Long> {

    Optional<User> findByUsername(String username);
}
