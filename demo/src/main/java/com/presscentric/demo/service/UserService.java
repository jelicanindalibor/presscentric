package com.presscentric.demo.service;

import com.presscentric.demo.dto.CreateUserInput;
import com.presscentric.demo.dto.UpdateUserInput;
import com.presscentric.demo.entity.User;
import com.presscentric.demo.entity.UserDetailsImpl;
import com.presscentric.demo.exception.EntityNotFoundException;
import com.presscentric.demo.exception.UserLoginException;
import com.presscentric.demo.repository.UserRepository;
import com.presscentric.demo.security.SecurityContext;
import com.presscentric.demo.security.jwt.JwtUtils;
import com.presscentric.demo.security.payload.request.LoginRequest;
import com.presscentric.demo.security.payload.response.LoginResponse;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService implements UserDetailsService {

    @Autowired
    private JpaQueryService jpaQueryService;

    @Autowired
    private UserRepository userRepository;



    @Value("${deep.platform.app.jwtExpirationMs}")
    private Long jwtExpirationMs;

    @Autowired
    JwtUtils jwtUtils;
    @Lazy
    @Autowired
    PasswordEncoder passwordEncoder;
    @Lazy
    @Autowired
    AuthenticationManager authenticationManager;


    public Iterable<User> getAllUsers() {
        return jpaQueryService.getAll(User.class);
    }

    @SneakyThrows
    public User getUserById(Long id) {
        return (User) jpaQueryService.getById(User.class, id).orElseThrow(() -> new EntityNotFoundException("User not found with id: " + id));
    }

    public User createUser(CreateUserInput input) {
        User user = new User();
        user.setEmail(input.getEmail());
        user.setName(input.getName());

        return (User) jpaQueryService.save(User.class, user);
    }

    @SneakyThrows
    public User updateUser(Long id, UpdateUserInput input) {
        User user = (User) jpaQueryService.getById(User.class, id).orElseThrow(() -> new EntityNotFoundException("User not found with id: " + id));
        user.setEmail(input.getEmail());
        user.setName(input.getName());

        return (User) jpaQueryService.save(User.class, user);
    }

    @SneakyThrows
    public User deleteUser(Long id) {
        User user = (User) jpaQueryService.getById(User.class, id).orElseThrow(() -> new EntityNotFoundException("User not found with id: " + id));
        jpaQueryService.delete(User.class, user);
        return user;
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        return UserDetailsImpl.build(user);
    }

    public LoginResponse login(LoginRequest request) {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtUtils.generateJwtToken(authentication);
            User user = SecurityContext.getUserDetailsImpl().getUser();

            return new LoginResponse(token, jwtExpirationMs);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UserLoginException("Bad credentials!");
        }
    }
}