package com.presscentric.demo.service;

import com.presscentric.demo.repository.JpaCrudRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class JpaQueryService<T, ID extends Serializable> {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private Map<String, JpaCrudRepository<T, ID>> repositoryMap;

    private JpaCrudRepository<T, ID> getRepository(Class<T> clazz) {
        String beanName = Character.toLowerCase(clazz.getSimpleName().charAt(0)) + clazz.getSimpleName().substring(1) + "Repository";
        return repositoryMap.get(beanName);
    }

    public List<T> getAll(Class<T> clazz) {
        return getRepository(clazz).findAll();
    }

    public Optional<T> getById(Class<T> clazz, ID id) {
        return getRepository(clazz).findById(id);
    }

    public T save(Class<T> clazz, T object) {
        return getRepository(clazz).save(object);
    }

    public void delete(Class<T> clazz, T object) {
        getRepository(clazz).delete(object);
    }

    public <T> List<T> buildSimpleSelectSqlQuery(String sql) {
        Query query = entityManager.createNativeQuery(sql.toString());
        return query.getResultList();
    }

    public <T> List<T> buildSimpleSelectSqlQuery(String sql, final Map<String, Object> parameters) {
        Query query = entityManager.createNativeQuery(sql.toString());
        parameters.forEach(query::setParameter);
        return query.getResultList();
    }

    public <T> List<T> buildTypedSelectQuery(String sql, Class<T> resultClass) {
        TypedQuery<T> query = (TypedQuery<T>) entityManager.createNativeQuery(sql, resultClass);
        return query.getResultList();
    }

    public <T> List<T> buildTypedSelectQuery(String sql, Class<T> clazz, Map<String, Object> parameters) {
        entityManager.clear();
        TypedQuery<T> query = (TypedQuery<T>) entityManager.createNativeQuery(sql, clazz);
        parameters.forEach(query::setParameter);
        return query.getResultList();
    }

    public int buildDeleteQuery(String sql, final Map<String, Object> parameters) {
        Query query = entityManager.createNativeQuery(sql);
        parameters.forEach(query::setParameter);
        return query.executeUpdate();
    }
}