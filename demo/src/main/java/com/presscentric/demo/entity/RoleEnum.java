package com.presscentric.demo.entity;

public enum RoleEnum {

    ADMIN("ROLE_ADMIN"),
    REGULAR("ROLE_REGULAR");

    private final String value;

    private RoleEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static RoleEnum fromValue(String value) {
        for (RoleEnum role : RoleEnum.values()) {
            if (role.getValue().equals(value))
                return role;
        }
        throw new IllegalArgumentException("Invalid Enum value = " + value);
    }

}
