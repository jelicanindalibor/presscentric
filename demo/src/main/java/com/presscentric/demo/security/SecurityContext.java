package com.presscentric.demo.security;

import com.presscentric.demo.entity.Role;
import com.presscentric.demo.entity.RoleEnum;
import com.presscentric.demo.entity.UserDetailsImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.stream.Collectors;

public class SecurityContext {

    public static UserDetailsImpl getUserDetailsImpl() {
        return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static List<Role> getRoles() {
        return getUserDetailsImpl().getUser().getRoles();
    }

    public static boolean containsRole(RoleEnum roleEnum) {
        for (Role role : getRoles()) {
            if (role.getName().equals(roleEnum.getValue()))
                return true;
        }
        return false;
    }

    public static List<String> getRolesAsString() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
    }

    public static List<GrantedAuthority> getRolesAsGrantedAuthorities() {
        return getUserDetailsImpl().getUser().getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    public static Long getUserId() {
        return getUserDetailsImpl().getUser().getId();
    }

    public static boolean isUserLoggedIn() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser") ? false : true;
    }
}
