INSERT INTO user_profile (email, name, username, password) VALUES ('john.doe@example.com', 'John Doe', 'johndoe', '$2a$12$LnFf0pcBSqO9b9aU3PqdL.9Yos.44sX5hys3IHXwtupEBlfgHBLZy');
INSERT INTO user_profile (email, name, username, password) VALUES ('jane.smith@example.com', 'Jane Smith', 'janesmith', '$2a$12$8tvIutiY9Lhu7m60zM25me.IBHZwqH15H4EUp2S/ceYxQjjJWbdZS');
INSERT INTO user_profile (email, name, username, password) VALUES ('james.bond@example.com', 'James Bond', 'jamesbond', '$2a$12$8tvIutiY9Lhu7m60zM25me.IBHZwqH15H4EUp2S/ceYxQjjJWbdZS');
INSERT INTO user_profile (email, name, username, password) VALUES ('lisa.simpson@example.com', 'Lisa Simpson', 'lisasimpson', '$2a$12$8tvIutiY9Lhu7m60zM25me.IBHZwqH15H4EUp2S/ceYxQjjJWbdZS');
INSERT INTO user_profile (email, name, username, password) VALUES ('mike.ross@example.com', 'Mike Ross', 'mikeross', '$2a$12$8tvIutiY9Lhu7m60zM25me.IBHZwqH15H4EUp2S/ceYxQjjJWbdZS');

INSERT INTO role (name) VALUES ('ROLE_ADMIN');
INSERT INTO role (name) VALUES ('ROLE_USER');

INSERT INTO user_profile_role (user_profile_id, role_id) VALUES (1, 1);
INSERT INTO user_profile_role (user_profile_id, role_id) VALUES (2, 1);
INSERT INTO user_profile_role (user_profile_id, role_id) VALUES (3, 1);
INSERT INTO user_profile_role (user_profile_id, role_id) VALUES (4, 2);
INSERT INTO user_profile_role (user_profile_id, role_id) VALUES (5, 2);