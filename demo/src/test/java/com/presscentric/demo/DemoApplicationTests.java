package com.presscentric.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.presscentric.demo.dto.CreateUserInput;
import com.presscentric.demo.dto.UpdateUserInput;
import com.presscentric.demo.entity.User;
import com.presscentric.demo.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.http.MediaType;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService userService;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void testGetUserById() throws Exception {
		User user = new User(1L, "john@example.com", "John Doe");

		when(userService.getUserById(1L)).thenReturn(user);

		mockMvc.perform(MockMvcRequestBuilders.post("/graphql")
						.content("{\"query\":\"{ user(id: 1) { id name email } }\"}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.user.id").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.user.name").value("John Doe"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.user.email").value("john@example.com"));
	}

	@Test
	public void testCreateUser() throws Exception {
		User newUser = new User(1L, "newuser@example.com", "New User");

		when(userService.createUser(any(CreateUserInput.class))).thenReturn(newUser);

		mockMvc.perform(MockMvcRequestBuilders.post("/graphql")
						.content("{\"query\":\"mutation { createUser(input: { email: \\\"newuser@example.com\\\", name: \\\"New User\\\" }) { id name email } }\"}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.createUser.id").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.createUser.name").value("New User"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.createUser.email").value("newuser@example.com"));
	}

	@Test
	public void testUpdateUser() throws Exception {
		User updatedUser = new User(1L, "updateduser@example.com", "Updated User");

		when(userService.updateUser(any(Long.class), any(UpdateUserInput.class))).thenReturn(updatedUser);

		mockMvc.perform(MockMvcRequestBuilders.post("/graphql")
						.content("{\"query\":\"mutation { updateUser(id: 1, input: { email: \\\"updateduser@example.com\\\", name: \\\"Updated User\\\" }) { id name email } }\"}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.updateUser.id").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.updateUser.name").value("Updated User"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.updateUser.email").value("updateduser@example.com"));
	}

	@Test
	public void testDeleteUser() throws Exception {
		User deletedUser = new User(1L, "john.doe@example.com", "John Doe");
		when(userService.deleteUser(any(Long.class))).thenReturn(deletedUser);

		mockMvc.perform(MockMvcRequestBuilders.post("/graphql")
						.content("{\"query\":\"mutation { deleteUser(id: 1) { id name email } }\"}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.deleteUser.id").value(1))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.deleteUser.name").value("John Doe"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.deleteUser.email").value("john.doe@example.com"));
	}
}